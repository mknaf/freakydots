#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

from random import randint
from PIL import Image, ImageDraw


def im_1(x, y, fname):
    """Write an image with alternating black and white pixels.
    """
    im = Image.new("RGBA", (x, y))
    draw = ImageDraw.Draw(im)

    for i in range(im.size[0]):
        for j in range(im.size[1]):

            if i % 2 and j % 2:
                draw.point((i, j), (0, 0, 0, 0))
            elif i % 2 and not j % 2:
                draw.point((i, j), "black")
            elif not i % 2 and j % 2:
                draw.point((i, j), "black")
            else:
                draw.point((i, j), (0, 0, 0, 0))
    del draw
    im.save(fname)


def im_2(x, y, fname):
    """Write an image with randomized black and white pixels.
    """
    im = Image.new("RGBA", (x, y))
    draw = ImageDraw.Draw(im)

    for i in range(im.size[0]):
        for j in range(im.size[1]):
            if randint(0, 1):
                draw.point((i, j), (0, 0, 0, 0))
            else:
                draw.point((i, j), "black")

    del draw
    im.save(fname)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Draw images."
    )
    parser.add_argument(
        "-x",
        "--width",
        type=int,
        required=True,
        help="width"
    )
    parser.add_argument(
        "-y",
        "--height",
        type=int,
        required=True,
        help="height"
    )
    parser.add_argument(
        "-o",
        "--outfile",
        type=str,
        required=False,
        default="out.png",
        help="output file name. default: 'out.png'."
    )

    args = parser.parse_args()

    im_2(
        args.width,
        args.height,
        args.outfile
    )
