"use strict";

// activate nag
$("#legend").nag("show");

// listen to mouse button events
var intervalRotate = null;
// on mouse button press
$(".freakydots").mousedown(function(e) {
    // every 20ms, call function rotate(e)
    intervalRotate = setInterval(
        rotate.bind(null, e),
        20
    );
// on mouse button release
});
$("body").mouseup(function(e) {
    // stop rotate(e) interval call
    clearInterval(intervalRotate);
});
// deactivate context menu (left mouse button)
$(".freakydots").contextmenu(function(e) {
    return false;
});

// rotate #fg element - use this with setInterval
var rot = 0;
function rotate(e) {
    switch(e.which) {
        case 1:  // left mouse button
            rot = rot - .1;
            break;
        case 3:  // right mouse button
            rot = rot + .1;
            break;
        default:  // anything else
            return;
    }
    e.preventDefault();

    rot = rot % 360;

    $("#fg").css("transform", "rotate(" + rot + "deg)");
};


// listen to mousewheel scroll events
var zoom = parseInt($("#fg").css("background-size").split("%")[0]);
$(".freakydots").bind("mousewheel DOMMouseScroll", function(e) {
    if (e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
        // mwheel scrolled up
        zoom = zoom - 1;
    } else {
        // mwheel scrolled down
        zoom = zoom + 1;
    }
    $("#fg").css("background-size", zoom + "% auto");
});


// listen to mouse move events
var par_fact = -0.1;
var lastEvent = $.now();
$("body").mousemove(function(e) {

    // only do stuff every n ms
    if ($.now() - lastEvent < 30) {
        return;
    }
    lastEvent = $.now();

    // viewport width - query here in case of window size change
    var vpWidth = $(window).width();
    var vpHeight = $(window).height();

    // current mouse position
    var mouseX = e.pageX;
    var mouseY = e.pageY;

    // set the new #fg X position
    var newX = par_fact * (0.5 * vpWidth - mouseX);
    $("#fg").css("left", newX + "px");

    // set the new #fg Y position
    var newY = par_fact * (0.5 * vpHeight - mouseY);
    $("#fg").css("top", newY + "px");


    // re-show the nag when mouse is close to top of window
    if (mouseY < 20) {
        $("#legend").nag("show");
    }
});


// keydown listeners
$(window).keydown(function(e) {
    switch(e.which) {

        // change images
        case 49:  // 1
            $("#fg, #bg").css("background-image", "url('img/out.png')");
            break;
        case 50:  // 2
            $("#fg, #bg").css("background-image", "url('img/rand.png')");
            break;
        case 51:  // 3
            $("#fg, #bg").css("background-image", "url('img/rand_2.png')");
            break;
        case 52:  // 4
            $("#fg, #bg").css("background-image", "url('img/rand_3.png')");
            break;
        case 53:  // 5
            $("#fg, #bg").css("background-image", "url('img/rand_4.png')");
            break;


        // reset display
        case 82:  // r
            // reset zoom
            zoom = parseInt($("#bg").css("background-size").split("%")[0]);
            $("#fg").css("background-size", zoom + "% auto");

            // reset rotation
            rot = 0;
            $("#fg").css("transform", "rotate(" + rot + "deg)");

            break;

        default:
            return;
    };
});
